package utils;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.util.Properties;

import org.junit.Test;

public class UtilsTest {

	

	@Test
	public void testOpenConnection() throws Exception {
		Connection conn = Utils.openConnection();

		conn.close();
	}

	@Test
	public void testLoadProperties() throws Exception {
		//GIVEN
		Properties properties = Utils.loadProperties();

		//WHEN
		String url = properties.getProperty("url");

		//THEN
		assertEquals("jdbc:postgresql://localhost:5432/postgres", url);
	}

	

}

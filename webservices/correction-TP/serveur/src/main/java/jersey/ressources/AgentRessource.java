package jersey.ressources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import core.Agent;

@Path("agent")
public class AgentRessource {

	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/hello")
	public String sayHello() {
		return "<h1>Hello</h1>";
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Agent getAgent() {
		Agent agent = new Agent();
		agent.setIdep("F2WBNP");
		return agent;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/{idep}")
	public Response getAgentByIdep(@PathParam(value="idep") String idepAgent) {
		if ("XXXXXX".equals(idepAgent)) {
			return Response.status(404).entity("").build();
		}
		
		if (idepAgent.length() != 6) {
			return Response.status(400).entity("").build();
		}
	
		Agent agent = new Agent();
		agent.setIdep(idepAgent);
		return Response.ok(agent).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Path("/filtre")
	public List<Agent> getAgentByFiltre(@QueryParam(value="categorie") int categorie) {
		List<Agent> agents = new ArrayList<>();
		Agent agent = new Agent();
		agent.setCategorie(categorie);
		Agent agent2 = new Agent();
		agent2.setCategorie(categorie);
		agents.add(agent);
		agents.add(agent2);
		return agents;
	}
	
	@POST
	@Path("ajouter")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Agent addAgent(Agent agent) {
		return agent;
	}
	
}

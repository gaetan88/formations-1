<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Formation Webservices
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

[Olivier Levitt](https://bitbucket.org/olevitt)  (olivier.levitt@gmail.com)

Online : [http://formations.levitt.fr/webservices](http://formations.levitt.fr/webservices)  
Source : [https://bitbucket.org/olevitt/formations](https://bitbucket.org/olevitt/formations)  
TP : [Enoncé](TP.html)  
Corrigé [sur le dépôt git](https://bitbucket.org/olevitt/formations/src/master/webservices/correction-TP) (git clone ou interface web)

Ecrit en [markdown][1] et propulsé par [reveal.js](http://lab.hakim.se/reveal-js/)

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
